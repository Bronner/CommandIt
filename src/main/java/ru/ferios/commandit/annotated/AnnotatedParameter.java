package ru.ferios.commandit.annotated;

import java.lang.annotation.Annotation;
import javax.annotation.Nullable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.ferios.commandit.CommandContext;
import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.ContextArgumentConsumer;
import ru.ferios.commandit.Parameter;
import ru.ferios.commandit.ParameterValidationException;
import ru.ferios.commandit.StringArgumentConsumer;

@Data
@EqualsAndHashCode(callSuper = true)
class AnnotatedParameter extends Parameter {

    private final Annotation[] annotations;

    private TypeParser parser;

    @Nullable
    private Annotation modifier;

    /**
     * Создает экземпляр класса.
     *
     * @param name название параметра
     * @param type тип параметра
     * @param annotations аннотации параметра
     */
    public AnnotatedParameter(String name, Class<?> type, Annotation[] annotations) {
        super(type, name);
        this.annotations = annotations;
    }

    @Nullable
    public Object getDefaultValue() {
        if (value == null) {
            if (type.isPrimitive()) {
                if (type == boolean.class || type == Boolean.class) {
                    value = "false";
                } else {
                    value = "0";
                }
            } else {
                return null;
            }
        }

        try {
            return parser.bind(this, new StringArgumentConsumer(value));
        } catch (CommandException e) {
            throw new ParameterValidationException(
                "The default value of the parameter using the binding " +
                    parser.getClass() + " in the method\n"
                //+ method.toGenericString() + "\nis invalid"
            );
        }
    }

    /**
     * Возвращает значение параметра на основе оргументов команды.
     * Если параметр является {@link Optional}
     * и значение не было указано, то будет возвращено значение по-умолчанию.
     *
     * @param args команда
     * @return значение параметра
     * @throws CommandException исключение
     */
    @Nullable
    public Object getValue(ContextArgumentConsumer args) throws CommandException {
        if (getFlag() != null) {
            CommandContext context = args.getContext();

            if (isValueFlag()) {
                return parser.bind(this, new StringArgumentConsumer(context.getFlag(getFlag())));
            } else {
                String bool = context.hasFlag(getFlag()) ? "true" : "false";
                return parser.bind(this, new StringArgumentConsumer(bool));
            }
        }
        try {
            return parser.bind(this, args);
        } catch (CommandException e) {
            if (!isOptional()) {
                throw e;
            }

            //            if (getDefaultValue() == null) {
            //                if (!getType().isPrimitive()) {
            //                    return null;
            //                }
            //                setDefaultValue("0");
            //            }
            //            return parser.bind(this, new StringArgumentConsumer(getDefaultValue()));
            return getDefaultValue();
        }
    }

    public void setParser(TypeParser parser) {
        this.parser = parser;
        final TypeMapping mapping = parser.getTypeMapping(this);
        setNumConsumes(mapping.numConsumes());
        setConsumer(mapping.consumer());
    }

    @Override
    public String toString() {
        if (getFlag() == null) {
            if (isOptional()) {
                return String.format("[%s]", getName());
            } else {
                return String.format("<%s>", getName());
            }
        } else {
            if (!isValueFlag()) {
                return String.format("[-%s]", getFlag());
            } else {
                return String.format("[-%s <%s>]", getFlag(), getName());
            }
        }
    }
}
