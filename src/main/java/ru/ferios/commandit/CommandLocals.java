package ru.ferios.commandit;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;

public class CommandLocals {

    private final Map<Object, Object> map = new HashMap<>();

    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Nullable
    public Object get(Object o) {
        return map.get(o);
    }

    @Nullable
    public <T> T get(Class<T> cls) {
        return cls.cast(map.get(cls));
    }

    public void put(Object key, Object value) {
        map.put(key, value);
    }
}
