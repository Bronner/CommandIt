package ru.ferios.commandit.annotated;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.CommandNotFoundException;
import ru.ferios.commandit.CommandPermissionException;
import ru.ferios.commandit.CommandUsageException;
import ru.ferios.commandit.InvalidUsageException;
import ru.ferios.commandit.book.Book;
import ru.ferios.commandit.book.BookCommand;
import ru.ferios.commandit.book.BookCompleter;
import ru.ferios.commandit.book.BookTypeParser;
import ru.ferios.commandit.dummy.CompletionCommands;
import ru.ferios.commandit.dummy.DummyCommand;

public class DispatcherTest {

    AnnotatedDispatcher dispatcher;

    @Before
    public void before() {
        dispatcher = new AnnotatedDispatcher();
    }

    @Test
    public void testCommandExecution() throws Exception {
        final DummyCommand cmd = new DummyCommand();
        dispatcher.register(cmd, "test1", "test2");

        assertTrue(dispatcher.call("test1 noarg"));
        assertTrue(dispatcher.call("test1 alias"));
        assertTrue(dispatcher.call("test2 testAlias"));
        assertTrue(dispatcher.call("test1 oneArgCommand passedValue"));
        assertTrue(dispatcher.call("test1 text this is a text"));
        assertTrue(dispatcher.call("test2 integer 123"));

        assertTrue(cmd.alias);
        assertTrue(cmd.noArg);
        assertEquals(cmd.oneArg, "passedValue");
        assertEquals(cmd.text, "this is a text");
        assertEquals(cmd.integer, 123);
    }

    @Test
    public void testCommandRegistration() {
        dispatcher.register(new DummyCommand());
        assertTrue(dispatcher.contains("alias"));
    }

    @Test(expected = InvalidUsageException.class)
    public void testMissingParameter() throws CommandException {
        dispatcher.register(new DummyCommand());
        dispatcher.call("integer");
    }

    @Test(expected = CommandUsageException.class)
    public void testMissingSubCommand() throws Exception {
        dispatcher.register(new DummyCommand(), "test");
        dispatcher.call("test");
    }

    @Test(expected = CommandNotFoundException.class)
    public void testNonexistentCommand() throws Exception {
        dispatcher.call("abc");
    }

    @Test(expected = CommandPermissionException.class)
    public void testPermission() throws Exception {
        final DummyCommand command = new DummyCommand();
        dispatcher.register(command);
        assertTrue(dispatcher.call("permission"));
    }

    @Test
    public void testTypeParser() throws Exception {
        BookCommand command = new BookCommand();

        dispatcher.addParser(new BookTypeParser());
        dispatcher.register(command, "books");

        assertTrue(dispatcher.call("books show PCs For Dummies"));
        assertEquals("PCs For Dummies", command.title);
    }

    @Test
    public void testValidation() {
        dispatcher.register(new DummyCommand());
        try {
            assertTrue(dispatcher.call("stringLen abc"));
            assertTrue(dispatcher.call("stringLen ABC123"));
            assertTrue(dispatcher.call("stringFormat ABC"));
            assertTrue(dispatcher.call("numLen 3"));
            assertTrue(dispatcher.call("numLen 6"));
        } catch (CommandException e) {
            fail();
        }

        try {
            dispatcher.call("stringLen a");
            fail();
        } catch (CommandException ignored) {
        }

        try {
            dispatcher.call("stringLen ABC123a");
            fail();
        } catch (CommandException ignored) {
        }

        try {
            dispatcher.call("stringFormat abc");
            fail();
        } catch (CommandException ignored) {
        }

        try {
            dispatcher.call("numLen 1");
            fail();
        } catch (CommandException ignored) {
        }

        try {
            dispatcher.call("numLen 7");
            fail();
        } catch (CommandException ignored) {
        }
    }

    @Test
    public void testOptionalSecond() throws CommandException {
        dispatcher.register(new DummyCommand());
        dispatcher.call("optionalSecond inputString");
    }

    @Test
    public void testOptionalFirst() throws CommandException {
        dispatcher.register(new DummyCommand());
        dispatcher.call("optionalFirst inputString");
    }

    @Test
    public void testOnlyFlag() throws CommandException {
        final DummyCommand cmd = new DummyCommand();
        dispatcher.register(cmd);
        dispatcher.call("onlyFlag -a");
        assertTrue(cmd.onlyFlag);
        dispatcher.call("onlyFlag");
        assertFalse(cmd.onlyFlag);
    }

    @Test
    public void testValueFlag() throws CommandException {
        final DummyCommand cmd = new DummyCommand();
        dispatcher.register(cmd);

        dispatcher.call("valueFlag");
        assertNull(cmd.valueFlagS);
        assertEquals(0, cmd.valueFlagI);

        dispatcher.call("valueFlag -s str123 -i 321");
        assertEquals("str123", cmd.valueFlagS);
        assertEquals(321, cmd.valueFlagI);

        try {
            dispatcher.call("valueFlag -i str");
            fail();
        } catch (CommandException ignored) {
        }
    }

    @Test
    public void testCompletionAll() throws Exception {
        dispatcher.register(new CompletionCommands());
        assertEquals(4, dispatcher.complete("").size());
    }

    @Test
    public void testCompletionPartialCommand() throws Exception {
        dispatcher.register(new CompletionCommands());
        assertEquals(2, dispatcher.complete("add").size());
    }

    @Test
    public void testCompletionAlias() throws Exception {
        dispatcher.register(new CompletionCommands(), "alias");
        assertEquals(1, dispatcher.complete("").size());
        assertEquals(4, dispatcher.complete("alias ").size());
    }

    @Test
    public void testCompletionParameter() throws Exception {
        dispatcher.addParser(new BookTypeParser());
        dispatcher.register(new BookCommand());
        dispatcher.register(Book.class, new BookCompleter());
        assertEquals(4, dispatcher.complete("show ").size());
        assertEquals(2, dispatcher.complete("show h").size());
    }
}
