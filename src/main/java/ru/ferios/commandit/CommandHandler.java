package ru.ferios.commandit;

/**
 * Предоставляет методы выполнения команды.
 */
public interface CommandHandler extends Completer {

    /**
     * Проверяет права на выполнение команды.
     *
     * @param commandLocals параметры
     * @return {@code true} если есть права на выполнение
     */
    boolean checkPermission(CommandLocals commandLocals);

    /**
     * Выполняет обработку и выполнение команды.
     * <p>
     * При выполнении команды могут выброситься следующие исключения:
     * <li>{@link CommandNotFoundException} - Команда не найдена</li>
     * <li>{@link CommandPermissionException} - Нет прав на выполнение</li>
     * <li>{@link CommandUsageException} - Ошибка использования команды</li>
     * <li>{@link CommandException} - Прочие ошибки</li>
     * <li>{@link InternalCommandException} - Обёртка для остальных исключений</li>
     * </p>
     *
     * @param commandLocals пользовательские параметры
     * @param consumer аргументы
     * @return {@code true} если команда выполнена, иначе {@code false}
     * @throws CommandException
     */
    boolean handle(CommandLocals commandLocals, ArgumentConsumer consumer) throws CommandException;

    /**
     * Получить описание команды.
     *
     * @return описание команды
     */
    Description getDescription();
}
