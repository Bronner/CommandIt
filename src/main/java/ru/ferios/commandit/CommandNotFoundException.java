package ru.ferios.commandit;

public class CommandNotFoundException extends CommandException {

    private static final long serialVersionUID = -325996093223439287L;
}
