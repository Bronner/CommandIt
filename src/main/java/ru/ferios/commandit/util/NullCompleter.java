package ru.ferios.commandit.util;

import java.util.Collection;
import java.util.Collections;

import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.CommandLocals;
import ru.ferios.commandit.Completer;

public final class NullCompleter implements Completer {

    @Override
    public Collection<String> getCompletion(CommandLocals locals, ArgumentConsumer consumer) {
        return Collections.emptyList();
    }
}
