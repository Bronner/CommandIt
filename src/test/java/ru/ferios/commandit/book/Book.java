package ru.ferios.commandit.book;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class Book {
    private final String title;
}
