package ru.ferios.commandit;

public enum Consumer {

    /**
     * Использует аргументы только из {@link ArgumentConsumer}
     */
    ARGUMENTS,

    /**
     * Использует аргументы только из {@link CommandLocals}
     */
    LOCALS
}
