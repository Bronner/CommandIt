package ru.ferios.commandit;

import java.util.Collection;

/**
 * Предоставляет метод для автозавершения команды по клавише Tab.
 */
public interface Completer {

    Collection<String> getCompletion(CommandLocals locals, ArgumentConsumer consumer) throws CommandException;

}
