package ru.ferios.commandit.annotated;

import java.lang.annotation.Annotation;

import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.ArgumentException;
import ru.ferios.commandit.Consumer;

final class StandardTypeParser extends TypeParser {

    private static Double validate(double value, Annotation... annotations)
    throws ArgumentException {
        for (Annotation annotation : annotations) {
            if (annotation instanceof Validate) {
                Validate validate = (Validate) annotation;
                if (value < validate.min()) {
                    throw new ArgumentException("Введенное значение меньше допустимого");
                }
                if (value > validate.max()) {
                    throw new ArgumentException("Введенное значение больше допустимого");
                }
            }
        }

        return value;
    }

    private static String validate(String value, Annotation... annotations) throws
        ArgumentException {
        for (Annotation annotation : annotations) {
            if (annotation instanceof Validate) {
                Validate validate = (Validate) annotation;
                if (value.length() < validate.min()) {
                    throw new ArgumentException("Длина строки меньше допустимого");
                }
                if (value.length() > validate.max()) {
                    throw new ArgumentException("Длина строки больше допустимого");
                }
                if (!validate.value().isEmpty() && !value.matches(validate.value())) {
                    throw new ArgumentException("Введенное значение не соответствует формату");
                }
            }
        }
        return value;
    }

    @TypeMapping(type = {Boolean.class, boolean.class},
                 consumer = Consumer.ARGUMENTS,
                 numConsumes = 1)
    public boolean getBoolean(ArgumentConsumer stack) throws ArgumentException {
        return stack.nextBoolean();
    }

    @TypeMapping(type = {Double.class, double.class},
                 consumer = Consumer.ARGUMENTS,
                 numConsumes = 1)
    public double getDouble(ArgumentConsumer stack, Annotation[] annotations)
    throws ArgumentException {
        return validate(stack.nextDouble(), annotations);
    }

    @TypeMapping(type = {Float.class, float.class},
                 consumer = Consumer.ARGUMENTS,
                 numConsumes = 1)
    public float getFloat(ArgumentConsumer stack, Annotation[] annotations) throws ArgumentException {
        return validate(stack.nextDouble(), annotations).floatValue();
    }

    @TypeMapping(type = {Integer.class, int.class},
                 consumer = Consumer.ARGUMENTS,
                 numConsumes = 1)
    public int getInteger(ArgumentConsumer stack, Annotation[] annotations) throws ArgumentException {

        return validate(stack.nextDouble(), annotations).intValue();
    }

    @TypeMapping(type = {Long.class, long.class},
                 consumer = Consumer.ARGUMENTS,
                 numConsumes = 1)
    public long getLong(ArgumentConsumer stack, Annotation[] annotations) throws ArgumentException {
        return validate(stack.nextDouble(), annotations).longValue();
    }

    @TypeMapping(type = String.class,
                 consumer = Consumer.ARGUMENTS,
                 numConsumes = 1)
    public String getString(ArgumentConsumer stack, Annotation[] annotations)
    throws ArgumentException {
        return validate(stack.next(), annotations);
    }

    @TypeMapping(type = String.class,
                 modifier = Text.class,
                 consumer = Consumer.ARGUMENTS)
    public String getText(ArgumentConsumer stack, Annotation[] annotations, Text text)
    throws ArgumentException {
        return validate(stack.remaining(), annotations);
    }
}
