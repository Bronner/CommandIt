package ru.ferios.commandit;

public class ArgumentException extends CommandException {

    private static final long serialVersionUID = -7370271116602937066L;

    public ArgumentException() {
        super();
    }

    public ArgumentException(String message) {
        super(message);
    }
}
