package ru.ferios.commandit.book;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;

import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.CommandLocals;
import ru.ferios.commandit.Completer;

public class BookCompleter implements Completer {

    private static final Set<String> BOOKS = ImmutableSet.of("Harry Potter", "Game of Thrones",
        "Green Mile", "Hunger Games");

    @Override
    public Collection<String> getCompletion(CommandLocals locals, ArgumentConsumer consumer)
    throws CommandException {
        final String next = consumer.next();
        return BOOKS.stream()
            .filter(book -> book.toLowerCase().startsWith(next.toLowerCase()))
            .collect(Collectors.toList());
    }
}
