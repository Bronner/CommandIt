package ru.ferios.commandit.annotated;

import lombok.Data;
import ru.ferios.commandit.CommandDetails;
import ru.ferios.commandit.CommandHandler;
import ru.ferios.commandit.Description;

@Data
final class CommandDetailsImpl implements CommandDetails {

    private final CommandHandler handler;

    private final String[] aliases;

    public CommandDetailsImpl(CommandHandler handler, String... aliases) {
        this.handler = handler;
        this.aliases = aliases;
    }

    @Override
    public String getPrimaryAlias() {
        return aliases[0];
    }

    @Override
    public Description getDescription() {
        return handler.getDescription();
    }

}
