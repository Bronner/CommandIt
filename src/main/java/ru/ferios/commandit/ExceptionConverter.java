package ru.ferios.commandit;

public interface ExceptionConverter {

    /**
     * Конвертирует исключения разных типов в один общий тип исключений.
     * Если обработчик для данного типа исключений не был найден, то ничего не происходит.
     *
     * @param t конвертируемое исключение
     * @throws CommandException
     */
    void convert(Throwable t) throws CommandException;
}
