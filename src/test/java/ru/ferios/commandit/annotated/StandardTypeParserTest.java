package ru.ferios.commandit.annotated;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.annotation.Annotation;

import org.junit.Test;

import ru.ferios.commandit.ArgumentException;
import ru.ferios.commandit.StringArgumentConsumer;

public class StandardTypeParserTest {

    StandardTypeParser parser = new StandardTypeParser();

    Text text;

    Annotation[] annotations = new Annotation[0];

    @Test
    public void getBoolean() throws Exception {
        assertTrue(parser.getBoolean(new StringArgumentConsumer("true")));
        assertFalse(parser.getBoolean(new StringArgumentConsumer("false")));
        try {
            parser.getBoolean(new StringArgumentConsumer("123"));
            fail();
        } catch (ArgumentException ignored) {
        }
    }

    @Test
    public void getDouble() throws Exception {
        assertEquals(1.2D, parser.getDouble(new StringArgumentConsumer("1.2"), annotations), 0);
        try {
            parser.getDouble(new StringArgumentConsumer("abc"), annotations);
            fail();
        } catch (ArgumentException ignored) {
        }
    }

    @Test
    public void getFloat() throws Exception {
        assertEquals(1.2F, parser.getFloat(new StringArgumentConsumer("1.2"), annotations), 0);
        try {
            parser.getFloat(new StringArgumentConsumer("abc"), annotations);
            fail();
        } catch (ArgumentException ignored) {
        }
    }

    @Test
    public void getInteger() throws Exception {
        assertEquals(12, parser.getInteger(new StringArgumentConsumer("12"), annotations));
        try {
            parser.getInteger(new StringArgumentConsumer("abc"), annotations);
            fail();
        } catch (ArgumentException ignored) {
        }
    }

    @Test
    public void getLong() throws Exception {
        assertEquals(12L, parser.getLong(new StringArgumentConsumer("12"), annotations));
        try {
            parser.getLong(new StringArgumentConsumer("abc"), annotations);
            fail();
        } catch (ArgumentException ignored) {
        }
    }

    @Test
    public void getString() throws Exception {
        assertEquals("this", parser.getString(new StringArgumentConsumer("this is a test"), annotations));
    }

    @Test
    public void getText() throws Exception {
        final String text = "this is a test";
        assertEquals(text, parser.getText(new StringArgumentConsumer(text), annotations, this.text));
    }
}
