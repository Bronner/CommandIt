package ru.ferios.commandit.annotated;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Позволяет делать проверку значений числовых и текстовых параметров.
 * Таких, как диапазон значений, длинна или регулярные веражения.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Validate {

    /**
     * Максимальное значение числа или длины строки.
     *
     * @return максимальное значение
     */
    double max() default Double.MAX_VALUE;

    /**
     * Минимальное значение числа или длины строки.
     *
     * @return минимальное значение
     */
    double min() default Double.MIN_VALUE;

    /**
     * Регулярное выражение для проверки строк.
     *
     * @return регулярное выражение
     * @see java.util.regex.Pattern
     */
    String value() default "";
}
