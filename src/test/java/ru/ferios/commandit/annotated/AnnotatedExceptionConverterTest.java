package ru.ferios.commandit.annotated;

import org.junit.Test;

import ru.ferios.commandit.ExceptionConverter;
import ru.ferios.commandit.InternalCommandException;

public class AnnotatedExceptionConverterTest {

    @Test(expected = ConvertedException.class)
    public void convert() throws Throwable {
        ExceptionConverter converter = new TestConverter();
        try {
            converter.convert(new TestException());
        } catch (InternalCommandException e) {
            throw e.getCause();
        }
    }

    private static class TestConverter extends AnnotatedExceptionConverter {

        @ConvertException
        public void convert(TestException e) throws ConvertedException {
            throw new ConvertedException();
        }
    }

    private static class TestException extends Exception {

        private static final long serialVersionUID = -6066230849422948176L;
    }

    private static class ConvertedException extends Exception {

        private static final long serialVersionUID = -9159592933115010995L;
    }
}
