package ru.ferios.commandit;

import ru.ferios.commandit.annotated.Command;

/**
 * Интерфейс отвечает за проверку прав при выполнении команды.
 *
 * <p>Если у команды определены права доступа см. {@link Command#permissions()},
 * то реализация данного интерфейса необходима для правильного определения
 * разрешения на выполнения команд.</p>
 */
public interface PermissionChecker {

    /**
     * Проверяет разрешение на выполнение команды.
     *
     * @param map Аргументы
     * @param permission Разрешение для проверки
     * @return {@code true} если имеет разрешение на выполнение, иначе {@code false}
     */
    boolean hasPermission(CommandLocals map, String permission);
}
