package ru.ferios.commandit;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

import ru.ferios.commandit.util.ArgumentUtil;

public class StringArgumentConsumer extends ArgumentConsumer {

    private final String[] args;

    public StringArgumentConsumer(String string) {
        this.args = ArgumentUtil.split(checkNotNull(string));
    }

    public boolean hasNext() {
        return nextIndex < args.length;
    }

    public String[] consumedArray() {
        return Arrays.copyOfRange(args, 0, nextIndex);
    }

    @Override
    public String consumed() {
        return ArgumentUtil.join(consumedArray());
    }

    @Override
    public int argsLength() {
        return args.length;
    }

    public String[] remainingArray() throws MissingArgumentException {
        try {
            return Arrays.copyOfRange(args, nextIndex, args.length);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new MissingArgumentException();
        }
    }

    @Override
    public String remaining() throws MissingArgumentException {
        return ArgumentUtil.join(remainingArray());
    }

    @Override
    protected String get(int index) throws MissingArgumentException {
        try {
            return args[nextIndex];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new MissingArgumentException();
        }
    }
}
