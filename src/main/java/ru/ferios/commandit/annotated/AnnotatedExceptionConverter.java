package ru.ferios.commandit.annotated;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.ExceptionConverter;
import ru.ferios.commandit.InternalCommandException;

@Slf4j
public abstract class AnnotatedExceptionConverter implements ExceptionConverter {

    private final List<ExceptionHandler> handlers = new ArrayList<>();

    /**
     * Анализирует подкласс на наличее методов с аннотацией {@link ConvertException}
     * и регистрирует их как обработчиков исключений.
     */
    public AnnotatedExceptionConverter() {
        for (Method method : getClass().getMethods()) {
            if (method.getAnnotation(ConvertException.class) == null) {
                continue;
            }

            final Class<?>[] params = method.getParameterTypes();
            if (params.length == 0) {
                log.warn("Метод {} содержит неверное количество параметров и был пробущен", method);
                continue;
            }

            if (Throwable.class.isAssignableFrom(params[0])) {
                @SuppressWarnings("unchecked")
                final Class<? extends Throwable> param = (Class<? extends Throwable>) params[0];
                handlers.add(new ExceptionHandler(param, method));
            }
        }

        Collections.sort(handlers);
    }

    @Override
    public void convert(Throwable t) throws CommandException {
        for (ExceptionHandler handler : handlers) {
            if (handler.getType().isAssignableFrom(t.getClass())) {
                try {
                    handler.getMethod().invoke(this, t);
                } catch (InvocationTargetException e) {
                    if (e.getCause() instanceof CommandException) {
                        throw ((CommandException) e.getCause());
                    }
                    throw new InternalCommandException(e.getCause());
                } catch (Exception e) {
                    throw new InternalCommandException(e);
                }
            }
        }
    }

    @Data
    private static class ExceptionHandler implements Comparable<ExceptionHandler> {

        private final Class<? extends Throwable> type;

        private final Method method;

        /**
         * Сравнение классов исключений для сортировки
         * от верхнего уровня к более низким, чтобы избежать ситуации, когда
         * конвертер общего типа исключений перехватывает все подклассы.
         *
         * @param o объект
         * @return 0 если равны, 1 если текущий класс является родительским,
         *     -1 если текущий класс является дочерним
         */
        @Override
        public int compareTo(ExceptionHandler o) {
            if (type.equals(o.type)) {
                return 0;
            } else if (type.isAssignableFrom(o.type)) {
                return 1;
            } else if (o.type.isAssignableFrom(type)) {
                return -1;
            } else {
                return type.getCanonicalName().compareTo(o.type.getCanonicalName());
            }
        }
    }
}
