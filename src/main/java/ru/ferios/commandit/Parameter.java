package ru.ferios.commandit;

import javax.annotation.Nullable;

import lombok.Data;

@Data
public class Parameter {

    protected final Class<?> type;

    protected final String name;

    protected String value;

    protected boolean optional;

    @Nullable
    protected Character flag;

    protected Consumer consumer;

    protected int numConsumes;

    public boolean isOptional() {
        return optional || isFlag();
    }

    public void setFlag(char flag) {
        this.flag = flag;
        this.optional = true;
    }

    public boolean isFlag() {
        return flag != null;
    }

    public boolean isValueFlag() {
        return isFlag() && type != boolean.class && type != Boolean.class;
    }

    public boolean isNonFlagConsumer() {
        return consumer != Consumer.LOCALS && !isValueFlag();
    }
}
