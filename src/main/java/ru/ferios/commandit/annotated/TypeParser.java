package ru.ferios.commandit.annotated;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.CommandException;

/**
 * Подклассы данного абстрактного класса содержат группы методов,
 * определенных аннотацией {@link TypeMapping}.
 * Эти методы преобразовывают переданный аргумент в тип, указанный в аннотации
 * и возвращают новое значение.
 */
@Slf4j
public abstract class TypeParser {

    private final Map<Type, ParseHandler> handlers = new HashMap<>();

    /**
     * Анализирует подкласс на наличие методов с аннотацией {@link TypeMapping}
     * и регистрирует их в роли парсеров объектов.
     */
    public TypeParser() {
        for (Method method : getClass().getMethods()) {
            TypeMapping mapping = method.getAnnotation(TypeMapping.class);
            if (mapping == null) {
                continue;
            }

            if (mapping.type().length == 0) {
                throw new TypeParseException(
                    "Не указан тип объекта для парсинга\n" + method.toGenericString());
            }

            boolean provideAnnotations = Stream.of(method.getParameterTypes())
                .anyMatch(p -> p == Annotation[].class);

            Class<? extends Annotation> modifier = null;
            if (!mapping.modifier().equals(Annotation.class)) {
                modifier = mapping.modifier();
            }

            for (Type type : mapping.type()) {
                ParseHandler parseHandler =
                    new ParseHandler(method, mapping, type, modifier, provideAnnotations);
                if (modifier == null) {
                    handlers.put(type, parseHandler);
                } else {
                    handlers.put(modifier, parseHandler);
                }
            }
        }
    }

    /**
     * Связывает значение из аргументов команды с
     * определенным типом и возвращает связанный объект.
     *
     * @param parameter параметр
     * @param consumer команда
     * @return связанный объект
     */
    public final Object bind(AnnotatedParameter parameter, ArgumentConsumer consumer)
    throws CommandException {
        ParseHandler parseHandler = getHandler(parameter);
        List<Object> args = new ArrayList<>();

        args.add(consumer);

        if (parseHandler.isProvideAnnotations()) {
            args.add(parameter.getAnnotations());
        }

        if (parseHandler.getClassifier() != null) {
            args.add(parameter.getModifier());
        }
        try {
            return parseHandler.getMethod().invoke(this, args.toArray());
        } catch (IllegalArgumentException e) {
            log.error("Ошибка при парсинге параметра: {}, с классификатором: {}, в методе: \n{}",
                parseHandler.getType(), parameter.getModifier(), parseHandler.getMethod());
            throw e;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof CommandException) {
                throw ((CommandException) e.getCause());
            }
            throw new RuntimeException(e);
        }
    }

    /**
     * Возвращает список доступных типов для парсинга.
     *
     * @return список доступных типов для парсинга.
     */
    public Set<Type> getTypes() {
        return handlers.keySet();
    }

    /**
     * Определяет метод, который отвечает за парсинг параметра данного типа.
     *
     * @param parameter параметр
     * @return метод для парсинга
     */
    private ParseHandler getHandler(AnnotatedParameter parameter) {
        final Type type = parameter.getType();
        final Annotation modifier = parameter.getModifier();
        ParseHandler result;

        if (modifier == null) {
            result = handlers.get(type);
        } else {
            result = handlers.get(modifier.annotationType());
        }

        if (result == null) {
            throw new TypeParseException("Неизвестный тип " + type);
        }

        return result;
    }

    /**
     *
     * @param parameter
     * @return
     */
    public TypeMapping getTypeMapping(AnnotatedParameter parameter) {
        return getHandler(parameter).getMapping();
    }

    /**
     * Класс представляет тип объекта, который будет парситься.
     */
    @Data
    private static class ParseHandler {

        private final Method method;

        private final TypeMapping mapping;

        private final Type type;

        private final boolean provideAnnotations;

        @Nullable
        private final Class<? extends Annotation> classifier;

        ParseHandler(Method method, TypeMapping mapping, Type type,
            @Nullable Class<? extends Annotation> classifier, boolean provideAnnotations) {
            this.method = method;
            this.mapping = mapping;
            this.type = type;
            this.classifier = classifier;
            this.provideAnnotations = provideAnnotations;
        }
    }
}

