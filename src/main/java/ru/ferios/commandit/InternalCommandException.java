package ru.ferios.commandit;

/**
 * Исключение-обёртка для всех проверяемых исключений,
 * детали которых пользователь не должен знать.
 */
public class InternalCommandException extends CommandException {

    private static final long serialVersionUID = 9189306669903398646L;

    public InternalCommandException(Throwable cause) {
        super(cause);
    }
}
