package ru.ferios.commandit.book;

import ru.ferios.commandit.annotated.Command;

public class BookCommand {

    public String title;

    @Command
    public void show(Book book) {
        this.title = book.getTitle();
    }
}
