package ru.ferios.commandit.annotated;

class TypeParseException extends RuntimeException {

    private static final long serialVersionUID = 2111820241522295054L;

    public TypeParseException(String message) {
        super(message);
    }

    public TypeParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
