package ru.ferios.commandit.annotated;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ru.ferios.commandit.Consumer;

/**
 * Указывает парсеру {@link TypeParser}, что метод является
 * обработчиком для преобразования типа параметра.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TypeMapping {

    /**
     * Модификатор определяет, как будут преобразовываться параметры
     * с одинаковыми типами. Например {@link String} и {@link Text}.
     *
     * @return модификатор
     */
    Class<? extends Annotation> modifier() default Annotation.class;

    /**
     * @return тип в который преобразуется параметр
     */
    Class<?>[] type();

    /**
     * @return Тип используемых парсером агрументов
     */
    Consumer consumer();

    /**
     * Количество аргументов, необходимое для установки значения параметра.
     *
     * @return -1 если значение неизвестно
     */
    int numConsumes() default -1;
}
