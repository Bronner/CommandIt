package ru.ferios.commandit.annotated;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Обозначает необязательный параметр.
 *
 * <p>Можно установить значение по-умолчанию, если этого не сделать,
 * параметр вернёт {@code null} для объектов и {@code 0} или {@code false}
 * для примитивных типов.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Optional {

    /**
     * Возвращает указанное значение, если значение параметра не было задано пользователем.
     *
     * @return указанное значение
     */
    String value() default "";
}
