package ru.ferios.commandit;

public class ParameterValidationException extends RuntimeException {

    private static final long serialVersionUID = 473145626836202799L;

    public ParameterValidationException() {
        super();
    }

    public ParameterValidationException(String message) {
        super(message);
    }

    public ParameterValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
