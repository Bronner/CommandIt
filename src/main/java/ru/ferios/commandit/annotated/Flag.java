package ru.ferios.commandit.annotated;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Указывает, что параметр типа {@code boolean} является флагом
 * и для изменения значения требует указания соответствующего аргумента при вызове команды.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Flag {

    /**
     * @return значение флага
     */
    char value();
}
