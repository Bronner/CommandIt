package ru.ferios.commandit;

import java.util.List;
import java.util.Optional;

/**
 * Описание команды
 */
public interface Description {

    /**
     * Получить список параметров команды
     *
     * @return список параметров
     */
    List<Parameter> getParameters();

    /**
     * Короткий текст описания команды.
     *
     * @return описание команды или {@link Optional#empty()} если не указано
     */
    default Optional<String> getDescription() {
        return Optional.empty();
    }

    /**
     * Длинный текст описания команды.
     *
     * @return описание команды или {@link Optional#empty()} если не указано
     */
    default Optional<String> getHelp() {
        return Optional.empty();
    }

    /**
     * Получить формат использования команлы.
     *
     * @return формат команды
     */
    String getUsage();

    /**
     * Получить список доступных прав, которые должен иметь игрок для выполнения команды.
     *
     * @return список прав
     */
    List<String> getPermissions();

}
