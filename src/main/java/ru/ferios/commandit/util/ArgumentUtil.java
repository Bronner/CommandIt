package ru.ferios.commandit.util;

import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

public final class ArgumentUtil {

    private static final Joiner SPACE_JOINER = Joiner.on(" ");

    private static final Splitter SPACE_SPLITTER = Splitter.on(" ");

    private ArgumentUtil() {
    }

    public static String join(String[] array) {
        return SPACE_JOINER.join(array);
    }

    public static String[] split(String string) {
        return split(string, false);
    }

    public static String[] split(String string, boolean omitEmpty) {
        Splitter splitter = SPACE_SPLITTER.trimResults();
        if (omitEmpty) {
            splitter = splitter.omitEmptyStrings();
        }
        final List<String> list = splitter.splitToList(string);
        return list.toArray(new String[list.size()]);
    }
}
