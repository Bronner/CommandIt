package ru.ferios.commandit.annotated;

import static com.google.common.base.Preconditions.checkNotNull;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.ferios.commandit.CommandDetails;
import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.CommandLocals;
import ru.ferios.commandit.CommandNotFoundException;
import ru.ferios.commandit.Completer;
import ru.ferios.commandit.Dispatcher;
import ru.ferios.commandit.ExceptionConverter;
import ru.ferios.commandit.PermissionChecker;
import ru.ferios.commandit.StringArgumentConsumer;
import ru.ferios.commandit.util.NullCompleter;
import ru.ferios.commandit.util.NullPermissionChecker;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

@Slf4j
public class AnnotatedDispatcher implements Dispatcher {

    private final Map<Type, TypeParser> parsers = new HashMap<>();

    private final NestedCommandHandler rootHandler = new NestedCommandHandler(this);

    private final List<ExceptionConverter> converters = new ArrayList<>();

    private final Map<Class<?>, Completer> completers = new HashMap<>();

    @Getter
    private PermissionChecker permissionChecker;

    @Getter
    private Completer defaultCompleter;

    /**
     * Создает экземпляр класса со стандартными параметрами.
     */
    public AnnotatedDispatcher() {
        this(new NullPermissionChecker(), new NullCompleter());
        log.warn("Внимание! Диспетчер комманд использует параметры по-умолчанию.");
    }

    /**
     * Создает экземпляр класса на основе переданных параметров.
     *
     * @param permissionChecker {@link PermissionChecker}
     * @param defaultCompleter {@link Completer}
     */
    public AnnotatedDispatcher(PermissionChecker permissionChecker, Completer defaultCompleter) {
        checkNotNull(permissionChecker, "permissionChecker");
        checkNotNull(defaultCompleter, "defaultCompleter");

        this.permissionChecker = permissionChecker;
        this.defaultCompleter = defaultCompleter;

        addParser(new StandardTypeParser());
    }

    @Override
    public void setDefaultCompleter(Completer defaultCompleter) {
        checkNotNull(defaultCompleter, "defaultCompleter");
        this.defaultCompleter = defaultCompleter;
    }

    @Override
    public void setPermissionChecker(PermissionChecker permissionChecker) {
        checkNotNull(permissionChecker, "permissionChecker");
        this.permissionChecker = permissionChecker;
    }

    @Override
    public void addExceptionConverter(ExceptionConverter converter) {
        if (converters.contains(converter)) {
            return;
        }
        converters.add(converter);
    }

    @Override
    public List<ExceptionConverter> getExceptionConverters() {
        return converters;
    }

    @Override
    public void addParser(TypeParser parser) {
        checkNotNull(parser, "type");
        for (Type type : parser.getTypes()) {
            parsers.put(type, parser);
        }
    }

    @Override
    @Nullable
    public TypeParser getParser(Type type) {
        return parsers.get(type);
    }

    @Override
    public boolean call(String command) throws CommandException {
        return call(new CommandLocals(), command);
    }

    @Override
    public boolean call(CommandLocals locals, String command) throws CommandException {
        return call(locals, command, false);
    }

    @Override
    public boolean call(CommandLocals locals, String command, boolean removeOperator)
    throws CommandException {
        checkNotNull(locals, "CommandLocals cannot be null");
        checkNotNull(command, "Command cannot be null");

        if (command.length() < 1) {
            throw new CommandNotFoundException();
        }

        if (removeOperator) {
            command = command.substring(1);
        }

        return rootHandler.handle(locals, new StringArgumentConsumer(command));
    }

    @Override
    public boolean contains(String alias) {
        return rootHandler.contains(alias);
    }

    @Override
    public Collection<String> complete(String command) throws CommandException {
        return complete(new CommandLocals(), command);
    }

    @Override
    public Collection<String> complete(CommandLocals locals, String command)
    throws CommandException {
        checkNotNull(locals, "CommandLocals cannot be null");
        if (command != null) {
            return rootHandler.getCompletion(locals, new StringArgumentConsumer(command));
        }
        return Collections.emptyList();
    }

    @Nullable
    @Override
    public CommandDetails get(String alias) {
        return rootHandler.get(alias);
    }

    @Override
    public Collection<CommandDetails> getCommands() {
        return rootHandler.getCommands();
    }

    @Override
    public void register(Object instance, String... aliases) {
        if (aliases.length > 0) {
            NestedCommandHandler childHandler = new NestedCommandHandler(this);
            childHandler.registerAnnotatedMethods(instance);
            rootHandler.registerHandler(childHandler, aliases);
        } else {
            rootHandler.registerAnnotatedMethods(instance);
        }
    }

    @Override
    public void register(Class<?> bindTo, Completer completer) {
        checkNotNull(bindTo);
        checkNotNull(completer);
        completers.put(bindTo, completer);
    }

    @Nullable
    @Override
    public Completer getCompleter(Class<?> type) {
        return completers.get(type);
    }
}
