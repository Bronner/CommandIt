package ru.ferios.commandit.dummy;

import ru.ferios.commandit.annotated.Command;
import ru.ferios.commandit.annotated.Flag;
import ru.ferios.commandit.annotated.Optional;
import ru.ferios.commandit.annotated.Text;
import ru.ferios.commandit.annotated.Validate;

public class DummyCommand {

    public boolean noArg;

    public boolean alias;

    public String oneArg;

    public String text;

    public int integer;

    public boolean onlyFlag;

    public String valueFlagS;

    public int valueFlagI;

    @Command
    public void noarg() {
        noArg = true;
    }

    @Command
    public void oneArgCommand(String arg) {
        oneArg = arg;
    }

    @Command("alias")
    public void testAlias() {
        alias = true;
    }

    @Command
    public void text(@Text String text) {
        this.text = text;
    }

    @Command
    public void integer(Integer integer) {
        this.integer = integer;
    }

    @Command(permissions = "test")
    public void permission() {
    }

    @Command
    public void numLen(@Validate(min = 3, max = 6) int arg) {
    }

    @Command
    public void stringFormat(@Validate("^[A-Z]*$") String arg) {
    }

    @Command
    public void stringLen(@Validate(min = 3, max = 6) String arg) {
    }

    @Command
    public void optionalFirst(@Optional("default") String optionalValue, String requiredValue) {
        System.out.println(
            "optionalValue = [" + optionalValue + "], requiredValue = [" + requiredValue + "]");
    }

    @Command
    public void optionalSecond(String requiredValue, @Optional("default") String optionalValue) {
        System.out.println(
            "requiredValue = [" + requiredValue + "], optionalValue = [" + optionalValue + "]");
    }

    @Command
    public void onlyFlag(@Flag('a') boolean allow) {
        onlyFlag = allow;
        System.out.println(allow);
    }

    @Command
    public void valueFlag(@Flag('s') String s, @Flag('i') int i) {
        System.out.println(valueFlagS = s);
        System.out.println(valueFlagI = i);
    }
}
