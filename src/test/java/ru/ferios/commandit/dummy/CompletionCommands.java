package ru.ferios.commandit.dummy;

import ru.ferios.commandit.annotated.Command;

public class CompletionCommands {

    @Command
    public void addMember() {
    }

    @Command
    public void removeMember() {
    }

    @Command
    public void addOwner() {
    }

    @Command
    private void removeOwner() {
    }
}
