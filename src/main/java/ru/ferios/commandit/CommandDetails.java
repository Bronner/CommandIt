package ru.ferios.commandit;

public interface CommandDetails {

    /**
     * @return Обработчик команды
     */
    CommandHandler getHandler();

    /**
     * @return Главный псевдоним команды
     */
    String getPrimaryAlias();

    /**
     * @return Список всех псевдонимов команды
     */
    String[] getAliases();

    /**
     * @return Описание команды
     */
    Description getDescription();
}
