package ru.ferios.commandit.book;

import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.ArgumentException;
import ru.ferios.commandit.Consumer;
import ru.ferios.commandit.annotated.TypeMapping;
import ru.ferios.commandit.annotated.TypeParser;

public class
BookTypeParser extends TypeParser {

    @TypeMapping(type = Book.class,
                 consumer = Consumer.ARGUMENTS)
    public Book getBook(ArgumentConsumer stack) throws ArgumentException {
        return new Book(stack.remaining());
    }
}

