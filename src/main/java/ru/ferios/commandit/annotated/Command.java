package ru.ferios.commandit.annotated;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация указывает, что данный метод является командой.
 *
 * <p>Название команды определяется из названия метода, а так же значений поля {@link #value}</p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Command {

    /**
     * Определяет псевдонимы команды.
     *
     * @return Псевдонимы команды
     */
    String[] value() default {};

    /**
     * Определяет нужно ли использовать имя метода, как название команды по-умолчанию.
     * По-умолчанию: {@code true}
     *
     * @return {@code true} если имя метода используется, как название команды
     */
    boolean useMethodName() default true;

    /**
     * Определяет права доступа команды.
     *
     * @return Разрешения команды
     */
    String[] permissions() default {};

    /**
     * Формат использования команды
     *
     * @return формат команды
     */
    String usage() default "";

    /**
     * Краткое описание команды
     *
     * @return краткое описание команды
     */
    String description() default "";

    /**
     * Длинное описание команды
     *
     * @return длинное описание команды
     */
    String help() default "";

}
