package ru.ferios.commandit;

public class CommandUsageException extends CommandException {

    private static final long serialVersionUID = 7090417731038636121L;

    public CommandUsageException(String message) {
        super(message);
    }
}
