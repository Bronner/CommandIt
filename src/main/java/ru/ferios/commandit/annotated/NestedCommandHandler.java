package ru.ferios.commandit.annotated;

import lombok.extern.slf4j.Slf4j;
import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.CommandDetails;
import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.CommandHandler;
import ru.ferios.commandit.CommandLocals;
import ru.ferios.commandit.CommandNotFoundException;
import ru.ferios.commandit.CommandPermissionException;
import ru.ferios.commandit.CommandUsageException;
import ru.ferios.commandit.Description;
import ru.ferios.commandit.Dispatcher;
import ru.ferios.commandit.InternalCommandException;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

@Slf4j
final class NestedCommandHandler implements CommandHandler {

    private final Dispatcher dispatcher;

    private final Map<String, CommandDetails> commands = new HashMap<>();

    private final Description description = new SimpleDescription();

    NestedCommandHandler(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public boolean checkPermission(CommandLocals commandLocals) {
        if (commands.isEmpty()) {
            return true;
        }

        for (CommandDetails details : commands.values()) {
            if (details.getHandler().checkPermission(commandLocals)) {
                return true;
            }
        }
        return false;
    }

    public boolean contains(String alias) {
        return commands.containsKey(alias.toLowerCase());
    }

    @Nullable
    public CommandDetails get(String alias) {
        return commands.get(alias.toLowerCase());
    }

    public Collection<CommandDetails> getCommands() {
        return Collections.unmodifiableCollection(commands.values());
    }

    @Override
    public boolean handle(CommandLocals commandLocals, ArgumentConsumer consumer)
    throws CommandException {
        if (!consumer.hasNext()) {
            throw new CommandUsageException("Пожалуйста укажите подкоманду");
        }

        if (!checkPermission(commandLocals)) {
            throw new CommandPermissionException();
        }

        String currentCommand = consumer.next();
        CommandDetails details = get(currentCommand);
        if (details == null) {
            throw new CommandNotFoundException();
        }

        try {
            return details.getHandler().handle(commandLocals, consumer);
        } catch (CommandException e) {
            throw e; // TODO: 25.04.2016 добавить информацию о вызове дочерних подкоманд
        } catch (Throwable t) {
            throw new InternalCommandException(t);
        }
    }

    @Override
    public Description getDescription() {
        return description;
    }

    public void registerAnnotatedMethods(Object instance) {
        for (Method method : instance.getClass().getDeclaredMethods()) {
            Command annotation = method.getAnnotation(Command.class);
            if (annotation != null) {
                List<String> aliases = new ArrayList<>();
                if (annotation.useMethodName()) {
                    aliases.add(method.getName().toLowerCase());
                }
                aliases.addAll(Arrays.asList(annotation.value()));

                registerHandler(
                    new AnnotatedCommandHandler(dispatcher, instance, method, annotation),
                    aliases.toArray(new String[aliases.size()]));
            }
        }
    }

    public void registerHandler(CommandHandler handler, String... aliases) {
        CommandDetails data = new CommandDetailsImpl(handler, aliases);

        for (String alias : aliases) {
            alias = alias.toLowerCase();
            if (commands.containsKey(alias)) {
                log.warn("Обнаружен конфликт команд. Команда с именем {} будет пропущена.", alias);
            } else {
                commands.put(alias, data);
            }
        }
    }

    @Override
    public Collection<String> getCompletion(CommandLocals locals, ArgumentConsumer consumer)
    throws CommandException {
        //Первый аргумент
        String subCommand = consumer.hasNext() ? consumer.next() : "";
        //Если есть второй аргумент
        if (consumer.hasNext()) {
            CommandDetails details = get(subCommand);
            if (details != null) {
                return details.getHandler().getCompletion(locals, consumer);
            }
        } else {
            return getCommands().stream().filter(details -> {
                if (subCommand.isEmpty()) {
                    return true;
                }
                for (String alias : details.getAliases()) {
                    if (alias.startsWith(subCommand)) {
                        return true;
                    }
                }
                return false;
            })
                .filter(details -> details.getHandler().checkPermission(locals))
                .map(CommandDetails::getPrimaryAlias)
                .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }
}
