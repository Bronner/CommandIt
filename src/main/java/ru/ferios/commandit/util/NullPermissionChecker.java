package ru.ferios.commandit.util;

import ru.ferios.commandit.CommandLocals;
import ru.ferios.commandit.PermissionChecker;

/**
 * Реализация {@link PermissionChecker} которая при проверке прав всегда возвращает {@code false}.
 */
public final class NullPermissionChecker implements PermissionChecker {

    @Override
    public boolean hasPermission(CommandLocals map, String permission) {
        return false;
    }
}
