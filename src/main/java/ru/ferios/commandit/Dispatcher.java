package ru.ferios.commandit;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nullable;

import ru.ferios.commandit.annotated.TypeParser;

/**
 * Интерфейс {@code Dispatcher} предоставляет возможность
 * выполнения определенного действия на основе введенной команды.
 */
public interface Dispatcher {

    void addExceptionConverter(ExceptionConverter converter);

    List<ExceptionConverter> getExceptionConverters();

    void addParser(TypeParser parser);

    @Nullable
    TypeParser getParser(Type type);

    void setDefaultCompleter(Completer defaultCompleter);

    Completer getDefaultCompleter();

    void setPermissionChecker(PermissionChecker permissionChecker);

    PermissionChecker getPermissionChecker();

    boolean call(CommandLocals locals, String command, boolean removeOperator) throws CommandException;

    boolean call(CommandLocals locals, String command) throws CommandException;

    boolean call(String command) throws CommandException;

    Collection<String> complete(String command) throws CommandException;

    Collection<String> complete(CommandLocals locals, String command) throws CommandException;

    void register(Object commandInstance, String... aliases);

    void register(Class<?> bindTo, Completer completer);

    Collection<CommandDetails> getCommands();

    @Nullable
    CommandDetails get(String commandName);

    boolean contains(String commandName);

    @Nullable
    Completer getCompleter(Class<?> type);
}
