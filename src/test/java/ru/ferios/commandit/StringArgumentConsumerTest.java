package ru.ferios.commandit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class StringArgumentConsumerTest {

    @Test
    public void testEmptyConsumer() throws Exception {
        ArgumentConsumer consumer = new StringArgumentConsumer("");
        assertTrue(consumer.hasNext());
        assertEquals("", consumer.next());
        assertEquals(1, consumer.argsLength());
        assertEquals(1, consumer.position());
        assertEquals("", consumer.consumed());
        assertEquals("", consumer.remaining());
    }

    @Test
    public void testConsumerWithArguments() throws Exception {
        ArgumentConsumer consumer = new StringArgumentConsumer("arg1 321");
        assertEquals(0, consumer.position());
        assertEquals(2, consumer.argsLength());
        //arg1

        assertTrue(consumer.hasNext());
        assertEquals("arg1", consumer.next());
        assertEquals(1, consumer.position());
        assertEquals("arg1", consumer.consumed());
        assertEquals("321", consumer.remaining());

        //321
        assertTrue(consumer.hasNext());
        assertEquals("321", consumer.next());
        assertEquals(2, consumer.position());
        assertEquals("arg1 321", consumer.consumed());
        assertEquals("", consumer.remaining());

        //nothing
        assertFalse(consumer.hasNext());
        try {
            assertNull(consumer.next());
            fail();
        } catch (MissingArgumentException ignored) {
        }
        assertEquals(2, consumer.position());
        assertEquals("arg1 321", consumer.consumed());
        assertEquals("", consumer.remaining());
    }

}
