package ru.ferios.commandit.annotated;

import com.google.common.collect.ImmutableList;
import lombok.Data;
import ru.ferios.commandit.Description;
import ru.ferios.commandit.Parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
class SimpleDescription implements Description {

    private List<Parameter> parameters = new ArrayList<Parameter>();

    private List<String> permissions = new ArrayList<String>();

    private String description;

    private String help;

    private String overrideUsage;

    @Override
    public List<Parameter> getParameters() {
        return ImmutableList.copyOf(parameters);
    }

    @Override
    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }

    @Override
    public Optional<String> getHelp() {
        return Optional.ofNullable(help);
    }

    @Override
    public String getUsage() {
        if (overrideUsage != null) {
            return overrideUsage;
        }

        StringBuilder builder = new StringBuilder();
        boolean first = true;

        for (Parameter parameter : parameters) {
            if (!first) {
                builder.append(" ");
            }
            builder.append(parameter);
            first = false;
        }

        return builder.toString();
    }

    @Override
    public List<String> getPermissions() {
        return ImmutableList.copyOf(permissions);
    }
}
