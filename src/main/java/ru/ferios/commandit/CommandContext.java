package ru.ferios.commandit;

import lombok.Getter;
import ru.ferios.commandit.util.ArgumentUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

public class CommandContext {

    private final String calledCommand;

    private final String[] originalArgs;

    private final List<String> parsedArgs;

    private final Set<Character> booleanFlags = new HashSet<>();

    private final Map<Character, String> valueFlags = new HashMap<>();

    @Getter
    private final CommandLocals locals;

    CommandContext(@Nullable String calledCommand, String args,
            @Nullable Set<Character> valueFlags, @Nullable CommandLocals locals) throws CommandException {
        this(calledCommand, ArgumentUtil.split(args), valueFlags, locals);
    }

    /**
     * @param calledCommand
     * @param args
     * @param valueFlags флаги, для которых ожидается значение
     * @param locals
     * @throws CommandException
     */
    public CommandContext(@Nullable String calledCommand, String[] args,
            @Nullable Set<Character> valueFlags, @Nullable CommandLocals locals) throws CommandException {
        this.calledCommand = calledCommand;
        this.originalArgs = args;
        this.locals = locals != null ? locals : new CommandLocals();
        if (valueFlags == null) {
            valueFlags = Collections.emptySet();
        }
        parsedArgs = new ArrayList<>(originalArgs.length);

        for (int argIndex = 0; argIndex < originalArgs.length; ) {
            String arg = originalArgs[argIndex++];
            if (arg.length() > 1 && '-' == arg.charAt(0)) {
                if (!arg.matches("^-[a-zA-Z]+$")) {
                    throw new CommandException("invalid flag " + arg);
                }

                for (int i = 1; i < arg.length(); i++) {
                    char flag = arg.charAt(i);

                    if (this.valueFlags.containsKey(flag) || booleanFlags.contains(flag)) {
                        throw new CommandException("Флаг " + flag + " уже указан");
                    }

                    if (valueFlags.contains(flag)) {
                        if (argIndex >= originalArgs.length) {
                            throw new CommandException("Не указано значение для флага -" + flag);
                        }
                        this.valueFlags.put(flag, originalArgs[argIndex++]);
                    } else {
                        booleanFlags.add(flag);
                    }
                }
            } else {
                parsedArgs.add(arg);
            }
        }
    }

    public double getDouble(int index) throws NumberFormatException {
        return Double.parseDouble(parsedArgs.get(index));
    }

    public double getDouble(int index, double defaultValue) throws NumberFormatException {
        return index < parsedArgs.size() ? Double.parseDouble(parsedArgs.get(index)) : defaultValue;
    }

    public int getInteger(int index) throws NumberFormatException {
        return Integer.parseInt(parsedArgs.get(index));
    }

    public int getInteger(int index, int defaultValue) throws NumberFormatException {
        return index < parsedArgs.size() ? Integer.parseInt(parsedArgs.get(index)) : defaultValue;
    }

    public String getString(int index) {
        return parsedArgs.get(index);
    }

    public String getString(int index, String defaultVal) {
        return index < parsedArgs.size() ? parsedArgs.get(index) : defaultVal;
    }

    public String getString(int startIndex, int endIndex) {
        String[] result =
                parsedArgs.subList(startIndex, endIndex).toArray(new String[endIndex - startIndex]);
        return ArgumentUtil.join(result);
    }

    public String getJoinedArgs(int fromIndex) {
        return getString(fromIndex, parsedArgs.size());
    }

    public int argsLength() {
        return parsedArgs.size();
    }

    public boolean hasFlag(char ch) {
        return booleanFlags.contains(ch) || valueFlags.containsKey(ch);
    }

    public Set<Character> getFlags() {
        return booleanFlags;
    }

    public Map<Character, String> getValueFlags() {
        return valueFlags;
    }

    public String getFlag(char ch) {
        return valueFlags.get(ch);
    }

    public String getFlag(char ch, String defaultVal) {
        final String value = valueFlags.get(ch);
        if (value == null) {
            return defaultVal;
        }

        return value;
    }

    public int getFlagInteger(char ch) throws NumberFormatException {
        return Integer.parseInt(valueFlags.get(ch));
    }

    public int getFlagInteger(char ch, int defaultVal) throws NumberFormatException {
        final String value = valueFlags.get(ch);
        if (value == null) {
            return defaultVal;
        }

        return Integer.parseInt(value);
    }

    public double getFlagDouble(char ch) throws NumberFormatException {
        return Double.parseDouble(valueFlags.get(ch));
    }

    public double getFlagDouble(char ch, double defaultVal) throws NumberFormatException {
        final String value = valueFlags.get(ch);
        if (value == null) {
            return defaultVal;
        }

        return Double.parseDouble(value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Command: \n");
        sb.append("\t-");
        sb.append(ArgumentUtil.join(parsedArgs.toArray(new String[parsedArgs.size()])));
        sb.append("\n");

        sb.append("Flags: \n");
        for (Character booleanFlag : booleanFlags) {
            sb.append("\t-").append(booleanFlag);
        }
        for (Entry<Character, String> entry : valueFlags.entrySet()) {
            sb.append("\t-").append(entry.getKey()).append(" ").append(entry.getValue());
        }

        return sb.toString();
    }

    public static final class Builder {

        private String calledCommand;

        private String[] args;

        private Set<Character> valueFlags;

        private CommandLocals data;

        public CommandContext build(String args)
                throws CommandException {
            return args(args).build();
        }

        public CommandContext build(String args, Set<Character> valueFlags)
                throws CommandException {
            return args(args).flags(valueFlags).build();
        }

        public CommandContext build() throws CommandException {
            return new CommandContext(calledCommand, args, valueFlags, data);
        }

        public Builder command(String calledCommand) {
            this.calledCommand = calledCommand;
            return this;
        }

        public Builder args(String args) {
            args(ArgumentUtil.split(args));
            return this;
        }

        public Builder args(String[] args) {
            this.args = args;
            return this;
        }

        public Builder flags(Character... flags) {
            valueFlags = new HashSet<>(Arrays.asList(flags));
            return this;
        }

        public Builder flags(Set<Character> valueFlags) {
            this.valueFlags = new HashSet<>(valueFlags);
            return this;
        }

        public Builder data(CommandLocals data) {
            this.data = data;
            return this;
        }
    }
}
