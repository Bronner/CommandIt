package ru.ferios.commandit.annotated;

import lombok.Getter;
import ru.ferios.commandit.ArgumentConsumer;
import ru.ferios.commandit.ArgumentException;
import ru.ferios.commandit.CommandContext;
import ru.ferios.commandit.CommandException;
import ru.ferios.commandit.CommandHandler;
import ru.ferios.commandit.CommandLocals;
import ru.ferios.commandit.CommandPermissionException;
import ru.ferios.commandit.Completer;
import ru.ferios.commandit.ContextArgumentConsumer;
import ru.ferios.commandit.Dispatcher;
import ru.ferios.commandit.ExceptionConverter;
import ru.ferios.commandit.InternalCommandException;
import ru.ferios.commandit.InvalidUsageException;
import ru.ferios.commandit.MissingArgumentException;
import ru.ferios.commandit.ParameterValidationException;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

final class AnnotatedCommandHandler implements CommandHandler {

    private final Dispatcher dispatcher;

    private final Object commandInstance;

    private final Method method;

    private final String[] permissions;

    private final AnnotatedParameter[] parameters;

    private final Set<Character> valueFlags = new HashSet<>();

    @Getter
    private final SimpleDescription description = new SimpleDescription();

    public AnnotatedCommandHandler(Dispatcher dispatcher, Object commandInstance, Method method, Command annotation) {
        this.dispatcher = dispatcher;
        this.commandInstance = commandInstance;
        this.method = method;
        this.permissions = annotation.permissions();

        description.setPermissions(Arrays.asList(permissions));
        if (!annotation.usage().isEmpty()) {
            description.setOverrideUsage(annotation.usage());
        }
        if (!annotation.description().isEmpty()) {
            description.setDescription(annotation.description());
        }
        if (!annotation.help().isEmpty()) {
            description.setHelp(annotation.help());
        }

        Parameter[] methodPars = method.getParameters();
        parameters = new AnnotatedParameter[methodPars.length];

        byte numOptional = 0;

        for (int i = 0; i < methodPars.length; i++) {
            final AnnotatedParameter parameter = createParameter(methodPars[i]);
            //todo parameter.validate
            if (parameter.isOptional() && !parameter.isFlag()) {
                numOptional++;
            } else {
                validateRequiredParameter(numOptional, parameter, method);
            }
            parameters[i] = parameter;
        }

        description.setParameters(Arrays.asList(parameters));
    }

    @Override
    public boolean checkPermission(CommandLocals commandLocals) {
        if (permissions.length > 0) {
            for (String permission : permissions) {
                if (dispatcher.getPermissionChecker().hasPermission(commandLocals, permission)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean handle(CommandLocals commandLocals, ArgumentConsumer consumer)
            throws CommandException {
        if (!checkPermission(commandLocals)) {
            throw new CommandPermissionException();
        }
        CommandContext context = new CommandContext.Builder()
                .command(consumer.consumed())
                .args(consumer.remaining())
                .flags(valueFlags)
                .data(commandLocals)
                .build();
        ContextArgumentConsumer stack = new ContextArgumentConsumer(context);

        AnnotatedParameter parameter = null;
        try {
            Object[] invokeArgs = new Object[parameters.length];
            for (int i = 0; i < invokeArgs.length; i++) {
                parameter = parameters[i];
                if (mayConsumeArgument(i, stack)) {
                    invokeArgs[i] = parameter.getValue(stack);
                } else {
                    invokeArgs[i] = parameter.getDefaultValue();
                }
            }
            method.invoke(commandInstance, invokeArgs);
            return true;
        } catch (MissingArgumentException e) {
            throw new InvalidUsageException(
                    "Укажите значение для параметра " + parameter.getName(), this);
        } catch (ArgumentException e) {
            throw new InvalidUsageException(
                    "Неверное значение параметра " + parameter.getName() + ". " + e.getMessage(), this);
        } catch (CommandException e) {
            throw e;
        } catch (InvocationTargetException e) {
            for (ExceptionConverter converter : dispatcher.getExceptionConverters()) {
                converter.convert(e.getCause());
            }
            throw new InternalCommandException(e.getCause());
        } catch (IllegalAccessException e) {
            throw new InternalCommandException(e);
        }
    }

    @Override
    public Collection<String> getCompletion(CommandLocals locals, ArgumentConsumer consumer)
            throws CommandException {
        Completer completer = null;
        int parameterIndex = consumer.argsLength() - consumer.position();
        if (parameterIndex <= parameters.length) {
            AnnotatedParameter parameter = parameters[--parameterIndex];
            completer = dispatcher.getCompleter(parameter.getType());
        }
        if (completer == null) {
            completer = dispatcher.getDefaultCompleter();
        }
        return completer.getCompletion(locals, consumer);
    }

    private boolean mayConsumeArgument(int parameterIndex, ContextArgumentConsumer consumer) {
        AnnotatedParameter parameter = parameters[parameterIndex];
        final CommandContext context = consumer.getContext();
        if (parameter.isOptional()) {
            if (parameter.getFlag() != null) {
                return !parameter.isValueFlag() || context.hasFlag(parameter.getFlag());
            } else {
                int unusedArgs = consumer.argsLength() - consumer.position();
                for (int i = parameterIndex; i < parameters.length; i++) {
                    if (parameters[i].isNonFlagConsumer() && !parameters[i].isOptional()) {
                        unusedArgs -= parameters[i].getNumConsumes();
                    }
                }

                //Пропуск опционального параметра
                if (unusedArgs < 1) {
                    return false;
                }
            }
        }
        return true;
    }

    private AnnotatedParameter createParameter(Parameter par) {
        Class<?> type = par.getType();
        Annotation[] annotations = par.getDeclaredAnnotations();
        AnnotatedParameter parameter = new AnnotatedParameter(par.getName(), type, annotations);
        TypeParser parser;

        for (Annotation annotation : annotations) {
            if (annotation instanceof Flag) {
                parameter.setFlag(((Flag) annotation).value());
                if (parameter.isValueFlag()) {
                    valueFlags.add(parameter.getFlag());
                }
            } else if (annotation instanceof Optional) {
                parameter.setOptional(true);
                String value = ((Optional) annotation).value();
                if (!value.isEmpty()) {
                    parameter.setValue(value);
                }
            } else if (parameter.getParser() == null) {
                parser = dispatcher.getParser(annotation.annotationType());
                if (parser != null) {
                    parameter.setParser(parser);
                    parameter.setModifier(annotation);
                }
            }
        }

        //Если тип параметра не был передан с помощью аннотации
        if (parameter.getParser() == null) {
            parser = dispatcher.getParser(type);
            if (parser == null) {
                throw new ParameterValidationException(
                        "Не указан обработчик для параметра типа " + type + " в методе команды\n"
                                + method.toGenericString());
            }
            parameter.setParser(parser);
        }

        return parameter;
    }

    /**
     * Проверяет было ли у обязательного параметра определено число необходимых для работы
     * аргументов, в случае, когда обязательный параметр стоит после опциональных параметров.
     *
     * @param requiredParam Параметр
     * @param method Метод
     * @throws ParameterValidationException Если количества аргументов недостаточно для создания
     * обязательного параметра
     */
    private void validateRequiredParameter(byte numOptional, AnnotatedParameter requiredParam,
            Method method)
            throws ParameterValidationException {
        if (numOptional > 0 && requiredParam.isNonFlagConsumer()) {
            if (requiredParam.getNumConsumes() < 0) {
                throw new ParameterValidationException("Для обязательного параметра, стоящего за "
                        + "опциональным, необходимо указать количество аргументов, которое он может "
                        + "использовать " + requiredParam.getParser().getClass().getCanonicalName()
                        + "\nв методе: " + method.toGenericString());
            }
        }
    }
}
