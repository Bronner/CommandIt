package ru.ferios.commandit;

public abstract class ArgumentConsumer {

    protected int nextIndex = 0;

    /**
     * Возвращает {@code true} при наличии доступных аргументов.
     *
     * @return {@code true} при наличии доступных аргументов
     */
    public abstract boolean hasNext();

    /**
     * Возвращает следующее значение оргумента типа String из контекста команды.
     *
     * @return следующее значение оргумента
     * @throws MissingArgumentException исключение
     */
    public String next() throws MissingArgumentException {
        final String result = get(nextIndex);
        nextIndex++;
        return result;
    }

    /**
     * Возвращает следующее значение оргумента типа Boolean из контекста команды.
     *
     * @return следующее значение оргумента
     * @throws MissingArgumentException
     */
    public Boolean nextBoolean() throws ArgumentException {
        String bool = next();
        if ("true".equalsIgnoreCase(bool) || "false".equalsIgnoreCase(bool)) {
            return Boolean.parseBoolean(bool);
        }
        throw new ArgumentException("Введенное значение должно быть формата boolean");
    }

    /**
     * Возвращает следующее значение оргумента типа Double из контекста команды.
     *
     * @return следующее значение оргумента
     * @throws MissingArgumentException
     */
    public Double nextDouble() throws ArgumentException {
        final String val = next();
        try {
            return Double.valueOf(val);
        } catch (NumberFormatException e) {
            throw new ArgumentException("Введенное значение '" + val + "' не является числом");
        }
    }

    /**
     * Возвращает следующее значение оргумента типа Integer из контекста команды.
     *
     * @return следующее значение оргумента
     * @throws MissingArgumentException
     */
    public Integer nextInt() throws ArgumentException {
        return nextDouble().intValue();
    }

    /**
     * Возвращает строку c оставшимися аргументов команды.
     *
     * @return строку c оставшимися аргументов команды или пустую строку.
     */
    public abstract String remaining() throws ArgumentException;

    /**
     * Возвращает строку уже использованных аргументов команды.
     *
     * @return строку уже использованных аргументов команды или пустую строку.
     */
    public abstract String consumed();

    /**
     * @return Возвращает число доступных оргументов
     */
    public abstract int argsLength();

    /**
     * @return Возвращает текущую позицицю аргумента
     */
    public int position() {
        return nextIndex;
    }

    /**
     * Получает значение определенного аргумента по его индексу.
     *
     * @param index Индекс
     * @return Значение
     */
    protected abstract String get(int index) throws MissingArgumentException;
}
