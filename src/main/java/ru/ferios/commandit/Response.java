package ru.ferios.commandit;

public class Response {

    private final Type type;

    private final String message;

    Response(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public static Response fail(String message) {
        return new Response(Type.FAIL, message);
    }

    public static Response ok(String message) {
        return new Response(Type.SUCCESS, message);
    }

    private enum Type {

        SUCCESS,

        FAIL

    }

}
