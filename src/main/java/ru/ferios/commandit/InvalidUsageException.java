package ru.ferios.commandit;

public class InvalidUsageException extends CommandException {

    private static final long serialVersionUID = 2356073787840017405L;

    private final CommandHandler handler;

    public InvalidUsageException(CommandHandler handler) {
        this.handler = handler;
    }

    public InvalidUsageException(String message, CommandHandler handler) {
        super(message);
        this.handler = handler;
    }
}
