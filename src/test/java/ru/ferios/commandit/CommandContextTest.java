package ru.ferios.commandit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class CommandContextTest {

    private static final String command = "test -abc hello world command 123 par";

    private static final Set<Character> valueFlags = new HashSet<>(Arrays.asList('a', 'c'));

    CommandContext context;

    @Before
    public void before() {
        try {
            context = new CommandContext.Builder().build(command, valueFlags);
        } catch (CommandException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testArgLength() {
        assertEquals(4, context.argsLength());
    }

    @Test
    public void testNumFlags() {
        assertEquals(1, context.getFlags().size());
        assertEquals(2, context.getValueFlags().size());
    }

    @Test(expected = CommandException.class)
    public void testMissingFlags() throws CommandException {
        final String failingCommand = "hello -abc world";
        new CommandContext.Builder().build(failingCommand, valueFlags);
    }

    @Test
    public void testArgs() {
        assertEquals("test", context.getString(0));
        assertEquals("command", context.getString(1));
        assertEquals("command 123 par", context.getJoinedArgs(1));
        assertEquals(123, context.getInteger(2));
    }

    @Test
    public void testFlags() {
        assertTrue(context.hasFlag('a'));
        assertTrue(context.hasFlag('b'));
        assertTrue(context.hasFlag('c'));
        assertFalse(context.hasFlag('d'));
        assertEquals("hello", context.getFlag('a'));
        assertEquals("world", context.getFlag('c'));
    }

    @Test
    public void testMultiSpace() {
        try {
            new CommandContext.Builder().build("a b   c");
        } catch (CommandException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testFlagPositions() {
        try {
            assertTrue(new CommandContext.Builder().build("a -b").hasFlag('b'));
            assertTrue(new CommandContext.Builder().build("a -b c").hasFlag('b'));
        } catch (CommandException e) {
            e.printStackTrace();
            fail();
        }
    }
}
