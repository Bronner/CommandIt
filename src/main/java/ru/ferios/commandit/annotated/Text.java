package ru.ferios.commandit.annotated;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Обозначает, что параметр типа {@link String}, обозначенный данной аннотацией
 * принимает на вход текст, а не отдельное слово.
 * Параметр с этой аннотацией должен стоять в самом конце списка параметров метода.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Text {
}
