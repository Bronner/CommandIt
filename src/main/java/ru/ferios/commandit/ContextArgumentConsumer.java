package ru.ferios.commandit;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Предоставляет возможность навигации по словам в строке, разделенной с помощью пробелов.
 */
public class ContextArgumentConsumer extends ArgumentConsumer {

    private final CommandContext context;

    /**
     * Создает объект на основе переданного контекста
     *
     * @param context контекст
     */
    public ContextArgumentConsumer(CommandContext context) {
        this.context = checkNotNull(context, "context");
    }

    public String consumed() {
        return context.getString(0, nextIndex);
    }

    @Override
    public int argsLength() {
        return context.argsLength();
    }

    @Override
    protected String get(int index) throws MissingArgumentException {
        try {
            return context.getString(nextIndex);
        } catch (IndexOutOfBoundsException e) {
            throw new MissingArgumentException();
        }
    }

    /**
     * Возвращает исходный контекст.
     *
     * @return контекст
     */
    public CommandContext getContext() {
        return context;
    }

    @Override
    public boolean hasNext() {
        return nextIndex < context.argsLength();
    }

    @Override
    public String remaining() throws MissingArgumentException {
        try {
            final String result = context.getJoinedArgs(nextIndex);
            nextIndex = context.argsLength();
            return result;
        } catch (IndexOutOfBoundsException e) {
            throw new MissingArgumentException();
        }
    }
}
